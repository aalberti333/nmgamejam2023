extends CanvasLayer

@export var bottombun_button : Button
@export var topbun_button : Button
@export var topbunnasty_button : Button
@export var lettuce_button : Button
@export var tomato_button : Button
@export var patty_button : Button
@export var herring_button : Button
@export var cheese_button : Button
@export var pickles_button : Button
@export var egg_button : Button
@export var points : Label
@export var progress_bar : ProgressBar
@export var timer : Timer
@export var order_randomizer : Node

@export var bottom : Sprite2D
@export var protein : Sprite2D
@export var ingredient1 : Sprite2D
@export var ingredient2 : Sprite2D
@export var ingredient3 : Sprite2D
@export var top : Sprite2D

@export var bottomAssembled : Sprite2D
@export var proteinAssembled : Sprite2D
@export var ingredient1Assembled : Sprite2D
@export var ingredient2Assembled : Sprite2D
@export var ingredient3Assembled : Sprite2D
@export var topAssembled : Sprite2D

@export var penalty_timer : Timer
@export var gameover_panel : Panel
@export var begingame_panel : Panel

@export var ingredientBottomBun: Sprite2D
@export var ingredientTopBun: Sprite2D
@export var ingredientNastyBun: Sprite2D
@export var ingredientCheese: Sprite2D
@export var ingredientEgg: Sprite2D
@export var ingredientLettuce: Sprite2D
@export var ingredientTomato: Sprite2D
@export var ingredientRedHerring: Sprite2D
@export var ingredientPickles: Sprite2D
@export var ingredientPatty: Sprite2D

@export var background_music : AudioStreamPlayer
@export var fail_sound : AudioStreamPlayer
@export var success_sound : AudioStreamPlayer

@export var game_state : Node
var single_state

# Timer vars for progress bar
var total_duration : float = 10.0
var decrement_amount = 17
var grand_total = 0

var all_buttons : Array[Button]

var buttonclick_state : Array[String] = []
var recipe_instructions : Dictionary = {
}

var recipe_instructions_list : Array[String] = ["bottombun", "patty", "tomato", "tomato", "cheese", "topbun"]

# Called when the node enters the scene tree for the first time.
func _ready():
	single_state = get_node("/root/GameState")
	
	if single_state.first_load == true:
		single_state.first_load = false
		begingame_panel.visible = true
	else:
		begingame_panel.visible = false
		
	recipe_instructions = {
		"bottombun": ingredientBottomBun,
		"topbun": ingredientTopBun,
		"topbunnasty": ingredientNastyBun,
		"lettuce": ingredientLettuce,
		"tomato": ingredientTomato,
		"patty": ingredientPatty,
		"herring": ingredientRedHerring,
		"cheese": ingredientCheese,
		"pickles": ingredientPickles,
		"egg": ingredientEgg
	}

	recipe_instructions_list = order_randomizer.get_order()
	assign_sprites(recipe_instructions_list)
	
	bottomAssembled.set_texture(null)
	proteinAssembled.set_texture(null)
	ingredient1Assembled.set_texture(null)
	ingredient2Assembled.set_texture(null)
	ingredient3Assembled.set_texture(null)
	topAssembled.set_texture(null)
	
	# Calculate the amount to reduce each time based on the timer's wait time
	var decrement_amount : float = progress_bar.max_value / (total_duration / timer.wait_time)
	
	# Connect the timer's timeout signal to our function
	#timer.connect("timeout", self, "_on_Timer_timeout")
	timer.connect("timeout", Callable(self, "_on_Timer_timeout"))
	
	if single_state.start_timer_immediately:
		timer.start()
	
	background_music.play(GameState.music_progress)
	
	all_buttons = [
		bottombun_button,
		topbun_button,
		topbunnasty_button,
		lettuce_button,
		tomato_button,
		patty_button,
		herring_button,
		cheese_button,
		pickles_button,
		egg_button
	]
	
	bottombun_button.pressed.connect(self._select_ingredient.bind(bottombun_button))
	topbun_button.pressed.connect(self._select_ingredient.bind(topbun_button))
	topbunnasty_button.pressed.connect(self._select_ingredient.bind(topbunnasty_button))
	lettuce_button.pressed.connect(self._select_ingredient.bind(lettuce_button))
	tomato_button.pressed.connect(self._select_ingredient.bind(tomato_button))
	patty_button.pressed.connect(self._select_ingredient.bind(patty_button))
	herring_button.pressed.connect(self._select_ingredient.bind(herring_button))
	cheese_button.pressed.connect(self._select_ingredient.bind(cheese_button))
	pickles_button.pressed.connect(self._select_ingredient.bind(pickles_button))
	egg_button.pressed.connect(self._select_ingredient.bind(egg_button))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if progress_bar.value <= 0:
		gameover_panel.visible = true
#	if grand_total != 0 and grand_total % 5 == 0:
#		decrement_amount += 2
	if background_music.playing == false:
		GameState.music_progress = 0.0
		background_music.play(GameState.music_progress)
	
	if single_state.start_timer_immediately == true:
		single_state.start_timer_immediately = false
		timer.start()
	
func _penalty():
	for button in all_buttons:
		button.disabled = true
	penalty_timer.wait_time = 1.6
	penalty_timer.start()
	penalty_timer.connect("timeout", Callable(self, "_on_PenaltyTimer_timeout"))
	fail_sound.play()
	print("BAAAAAD")
	
func _successful_assembly():
	print("YOU DID IT!!!")
	success_sound.play()
	points.text = str(int(points.text) + 1)
	buttonclick_state.clear()
	k = 0
	grand_total += 1
	bottomAssembled.set_texture(null)
	proteinAssembled.set_texture(null)
	ingredient1Assembled.set_texture(null)
	ingredient2Assembled.set_texture(null)
	ingredient3Assembled.set_texture(null)
	topAssembled.set_texture(null)
	progress_bar.value = progress_bar.max_value
	recipe_instructions_list = order_randomizer.get_order()
	assign_sprites(recipe_instructions_list)
	timer.stop()
	timer.start()
	
var k = 0

func _select_ingredient(button) -> void:
	buttonclick_state.append(button.name)
	
	print(button.name)
	if buttonclick_state.size() <= recipe_instructions_list.size():
		for i in range(k, buttonclick_state.size()):
			if buttonclick_state[i] == recipe_instructions_list[i]:
				if i == 0:
					bottomAssembled.set_texture(recipe_instructions[button.name].get_texture())
				elif i == 1:
					proteinAssembled.set_texture(recipe_instructions[button.name].get_texture())
				elif i == 2:
					ingredient1Assembled.set_texture(recipe_instructions[button.name].get_texture())
				elif i == 3:
					ingredient2Assembled.set_texture(recipe_instructions[button.name].get_texture())
				elif i == 4:
					ingredient3Assembled.set_texture(recipe_instructions[button.name].get_texture())
				elif i == 5:
					topAssembled.set_texture(recipe_instructions[button.name].get_texture())
				k += 1
				continue
			else:
				_penalty()
				buttonclick_state.pop_back()
				break
		if(buttonclick_state == recipe_instructions_list):
			_successful_assembly()
	return
#	points.text = str(int(points.text) + 1)

func _on_Timer_timeout() -> void:
	# Reduce the progress bar value
	progress_bar.value -= decrement_amount
	
	# Stop the timer if the progress bar is empty
	if progress_bar.value <= 0:
		timer.stop()
		# ACTIVATE GAME OVER SCENE THING
		
func _on_PenaltyTimer_timeout() -> void:
	for button in all_buttons:
		button.disabled = false
	penalty_timer.stop()

func assign_sprites(recipe_instructions_list : Array[String]) -> void:
	print(recipe_instructions_list[5])
	print(recipe_instructions[recipe_instructions_list[5]])
	bottom.texture = recipe_instructions[recipe_instructions_list[0]].texture
	protein.texture = recipe_instructions[recipe_instructions_list[1]].texture
	ingredient1.texture = recipe_instructions[recipe_instructions_list[2]].texture
	ingredient2.texture = recipe_instructions[recipe_instructions_list[3]].texture
	ingredient3.texture = recipe_instructions[recipe_instructions_list[4]].texture
	top.set_texture(recipe_instructions[recipe_instructions_list[5]].get_texture())
