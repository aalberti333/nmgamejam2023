extends Panel

@export var start : Button
@export var quit : Button

# Called when the node enters the scene tree for the first time.
func _ready():
	start.pressed.connect(self._start_button_pressed)
	quit.pressed.connect(self._quit_button_pressed)

func _start_button_pressed():
	var single_state = get_node("/root/GameState")
	single_state.start_timer_immediately = true
	visible = false

func _quit_button_pressed():
	get_tree().quit()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
