extends Node

var recipe_instructions_list : Array[String]
const styles : Array[String] = ["bun", "nasty", "lowcarb", "salad"]
const proteins : Array[String] = ["herring", "patty"]
const ingredients : Array[String] = ["lettuce", "tomato", "cheese", "pickles", "egg"]

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _get_random_item(array: Array) -> Variant:
	randomize()
	var random_index = randi() % array.size()
	return array[random_index]
	
func get_order() -> Array[String]:
	return _randomize_style()
	
func _randomize_style() -> Array[String]:
	recipe_instructions_list.clear()
	for i in range(6):
		recipe_instructions_list.append("")
	
	var style : String = _get_random_item(styles)
	var protein : String = _get_random_item(proteins)
	
	var ingredient1 : String = _get_random_item(ingredients)
	var ingredient2 : String = _get_random_item(ingredients)
	var ingredient3 : String = _get_random_item(ingredients)
	
	if style == "bun":
		recipe_instructions_list[0] = "bottombun"
		recipe_instructions_list[1] = protein
		recipe_instructions_list[2] = ingredient1
		recipe_instructions_list[3] = ingredient2
		recipe_instructions_list[4] = ingredient3
		recipe_instructions_list[5] = "topbun"
	if style == "nasty":
		recipe_instructions_list[0] = "bottombun"
		recipe_instructions_list[1] = protein
		recipe_instructions_list[2] = ingredient1
		recipe_instructions_list[3] = ingredient2
		recipe_instructions_list[4] = ingredient3
		recipe_instructions_list[5] = "topbunnasty"
	if style == "lowcarb":
		recipe_instructions_list[0] = "lettuce"
		recipe_instructions_list[1] = protein
		recipe_instructions_list[2] = ingredient1
		recipe_instructions_list[3] = ingredient2
		recipe_instructions_list[4] = ingredient3
		recipe_instructions_list[5] = "lettuce"
	if style == "salad":
		recipe_instructions_list[0] = "lettuce"
		recipe_instructions_list[1] = "lettuce" # doesn't make sense with current layout in VBOX...fix? idc
		recipe_instructions_list[2] = "lettuce"
		recipe_instructions_list[3] = ingredient1
		recipe_instructions_list[4] = ingredient2
		recipe_instructions_list[5] = ingredient3
	return recipe_instructions_list
