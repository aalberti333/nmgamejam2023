extends Panel

@export var tryagain : Button
@export var quit : Button
@export var background_music: AudioStreamPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	tryagain.pressed.connect(self._tryagain_button_pressed)
	quit.pressed.connect(self._quit_button_pressed)

func _tryagain_button_pressed():
	var single_state = get_node("/root/GameState")
	single_state.start_timer_immediately = true
	
	# Save position of the current music, so on reload the music does not restart.
	single_state.music_progress = background_music.get_playback_position() 
	
	get_tree().reload_current_scene()

func _quit_button_pressed():
	get_tree().quit()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
